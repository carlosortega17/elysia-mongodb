import { getModelForClass, prop } from '@typegoose/typegoose';

export class Task {
  @prop({ type: () => String, unique: true })
  public title: string;
  @prop({ type: () => Boolean, default: false })
  public status: boolean;
}

export const TaskModel = getModelForClass(Task, {
  schemaOptions: { timestamps: true, versionKey: false },
});
