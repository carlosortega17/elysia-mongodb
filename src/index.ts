import { Elysia } from 'elysia';
import './config/mongoose';
import { TaskModel } from './model/task.model';

const app = new Elysia({ prefix: '/api'})
export type App = typeof app;

app.get('/', async () => {
  return {
    message: 'success',
    status: 200,
  };
});

app.get('/task', async () => {
  const tasks = await TaskModel.find().lean().exec();
  return tasks;
});

app.listen(4001);
