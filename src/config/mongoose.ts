import * as mongoose from "mongoose";

mongoose
  .connect(Bun.env.MONGO_URI ?? "mongodb://localhost:27017/mongo-api")
  .then(() => {
    console.log("Success connection with mongodb");
  });

process.on("SIGINT", () => {
  mongoose
    .disconnect()
    .then(() => {
      console.log("Success close mongodb connection");
      process.exit(0);
    })
    .catch((err) => {
      console.error(err);
      process.exit(1);
    });
});
